// the setup function runs once when you press reset or power the board
const int LEYE= A4; 
const int REYE= A3; 
int voltageL=0; 
int voltageR=0; 
const int threshold=400; 
const int TRIG = 9;
const int ECHO = 8;
int count =0;
const int encoder = 2;
volatile int counter = 0;

//everytime spoke is seen, LED flashes//
//5 spokes per revolution//



  
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(TRIG, OUTPUT);
  pinMode(encoder, INPUT);

  Serial.begin(9600);
  pinMode( LEYE , INPUT); 
  pinMode( REYE , INPUT); 




  attachInterrupt( digitalPinToInterrupt(encoder), ir_isr, RISING);

  /*digitalWrite(4, HIGH);
  analogWrite(5, LOW);
  analogWrite(6, HIGH);
  analogWrite(7, LOW);
   */                                                                              


}

void ir_isr(){
  counter = counter +1;


}

void us(int& dis, int dur){
 digitalWrite(TRIG,LOW);
 delayMicroseconds(2);

 digitalWrite(TRIG, HIGH);
 delayMicroseconds(10);
 digitalWrite(TRIG,LOW);
 dur = pulseIn(ECHO, HIGH);
 dis = dur/58;
}

// the loop function runs over and over again forever
void loop() {
  voltageL = analogRead( LEYE); 
  voltageR = analogRead( REYE); 


 //Serial.println(voltageL); 
 //Serial.println(voltageR); 

 



  
  

 if ( voltageL > threshold ) { 
  digitalWrite(4, HIGH);
  analogWrite(5, 140);  
  
 }
 
 else { 
   digitalWrite(4, HIGH);
   analogWrite(5, 230);
   analogWrite(6, 70);
   digitalWrite(7, HIGH);
   
 }  


 
 if (voltageL < threshold && voltageR < threshold){ 
  
  analogWrite(6, 150);
  digitalWrite(7, HIGH);
     
  digitalWrite(4, HIGH);
  analogWrite(5, 210);
   }  
 

 
 

 if ( voltageR > threshold ) { 
  analogWrite(6, 105);
  digitalWrite(7, HIGH);
     
 }
 
 else { 
   analogWrite(6, 210);
   digitalWrite(7, HIGH);
   digitalWrite(4, HIGH);
   analogWrite(5, 110);  
   
     
 }
 
 //Ultrasonic sensor//
 int distance;
 int duration;
 if(count%10 ==0){
 us(distance, duration);
 Serial.println(distance);
 while((distance <= 20) && (distance >=0)){
  digitalWrite(4, LOW);
  analogWrite(5, 0);  
  analogWrite(6, 0);
  digitalWrite(7, LOW);
  delay(100);
  us(distance, duration);
  Serial.println(distance);
 }
 Serial.println("Count: ");
 Serial.print(count);
 
 }
 count ++;
 
 
 
//erial.print("Count number: ");
//Serial.println( counter );
  

 delay(30); 
/* 
 if ( !all_clearR) { 
  Serial.println( " right i see you!"); 
 }
 else { 
  Serial.println("NO Right ");  
 } 
 delay(1000);*/


  
                    // wait for a second
}
